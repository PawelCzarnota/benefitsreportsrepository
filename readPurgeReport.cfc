<!--- readPurgeRPTReportFile.cfc

Purpose: 		this component contains functions that read Purge RPT Reports
Called by:		brrController.cfc
				brrDAO.cfc
Includes:		none
Modules:		none
CustomTags:		none
Components:		readPurgeRPTReport
	
Created:		24 July 2012 by Pawel Czarnota
Last Updated:	$Date: 2014-10-23 14:04:30 -0500 (Thu, 23 Oct 2014) $ by $Author: Pawel Czarnota $ - $Rev: 10427 $

SVN URL:		$URL: https://$svnbase/benefitsreportrepository/trunk/readPurgeReport.cfc $  
Decommissioned: 10/2020
--->

<cfcomponent name="readPurgeReport" hint="component to parse Purge RPT reports">

	<cffunction name="init" access="public" output="false" hint="Initialize and return readDiscrepancyReport object ">
		
        <cfreturn this />
	</cffunction><!--- END init --->

	<cffunction name="readPurgeReport" access="public" output="true" returntype="void" hint="reads data from Purge RPT Report">
    	<cfargument name="fileName" type="string" required="yes" hint="name of the report file" />
		<cfargument name="pathName" type="string" required="yes" hint="path to the report file" />
        <cfargument name="threadName" type="string" required="yes" hint="name of the thread this will run under" />
        
        <cfoutput>Reading Purge RPT Report File: #arguments.fileName# <br /> </cfoutput>
        
		<cfscript>	
			// the processing will be done in a thread
			
			// set up the attributes needed by the thread
			local.attributes = structNew();
			// add all the variables about the upload, the file, and the user
			structInsert(local.attributes, "startTime", now());
			structInsert(local.attributes, "arguments_fileName", arguments.fileName);
			structInsert(local.attributes, "arguments_pathName", arguments.pathName);
			
			//add some application and controller variables the thread will need
			structInsert(local.attributes, "BRRNotificationController", application.BRRNotificationController);
			structInsert(local.attributes, "TechSubject", "Error Processing #arguments.fileName# purge file");
			
			// set the other attributes of the thread 
			structInsert(local.attributes, "threadName", arguments.threadName);
			structInsert(local.attributes, "name", arguments.threadName);
			structInsert(local.attributes, "action", "run");
			// note, the priority is always low
			structInsert(local.attributes, "priority", "Low");
		</cfscript>	
        
       <cfthread attributeCollection="#local.attributes#">
            <cftry>  
            	<cfscript>
            		// this will tell us if there were any problems
					//	 It will be used to tell us if we should or should not send the final success message
					thread.processingError = false;
					thread.fileName = arguments_fileName;
					
					//this variable will be used to make the process sleep
					recordcount = 0;
					
				</cfscript>
              
                <cfloop file="#arguments_pathName#" index="local.line">
                    <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput>        
                
                	<cfset local.systemNotes="" />
                
                    <!--- 1. Check for page number --->
                    <cfset local.re = '^PROGRAM:.{2}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        
                        <cfset local.pageNo = Trim(Right(local.line,5)) />
                        <!---  account for a case when number is in 1,1234 format --->
                        <cfset local.pageNo = Replace(local.pageNo,",","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page: #local.pageNo#</cfoutput><br />
        
                        <cfcontinue />
                    </cfif>  
                
                    <!--- 2. Check for report number, program name, and report date --->
                    <cfset local.re = '^?REPORT:.{38}GROUP' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                    
                        <!--- extract report number from line --->
                        <cfset local.reportNo = Left(local.line,25) />
                        <cfset local.reportNo = Trim(Right(local.reportNo,15)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Number: #local.reportNo#</cfoutput><br />
                        
                        <!--- extract program name from line --->
                        <cfset local.programName = Left(local.line,115) />
                        <cfset local.programName = Trim(Right(local.programName,90)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Program Name: #local.programName#</cfoutput><br />
                        
                        <cfset local.reportDate = Left(local.line,131) />
                        <cfset local.reportDate = Trim(Right(local.reportDate,10)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Year: #Mid(local.reportDate,7,4)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Day: #Mid(local.reportDate,4,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Month: #Left(local.reportDate,2)#</cfoutput><br />
                        <cfset local.reportDate = CreateDate(Mid(local.reportDate,7,4),Left(local.reportDate,2),Mid(local.reportDate,4,2)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 3. Check for proc org and agencyName --->
                    <cfset local.re = '^\d{3}.\d{3}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract processing organization # --->
                        <cfset local.procOrg = Left(local.line,3) & "-" & Mid(local.line,5,3) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proc Org: #local.procOrg#</cfoutput><br />
                        
                        <!--- extract agency name --->
                        <cfset local.agencyName = Mid(local.line,9,Len(local.line)) /><!--- second parameter causes out of bounds but that's okay --->
                        <cfset local.agencyName = Trim(local.agencyName) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agency Name: #local.agencyName#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 4. Check for record information and store all information in data structure --->
                    <cfset local.re = '^XXX-XX-\d{4}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match eq 0>
                        <cfset local.re = '^\d{3}-\d{2}-\d{4}' />
                    </cfif>
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract SSN --->
                        <cfset local.SSN = Left(local.line,11) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SSN: #local.SSN#</cfoutput><br />
                        
                        <!--- remove dashes from SSB--->
                        <cfset local.SSN = Replace(local.SSN,"-","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SSN: #local.SSN#</cfoutput><br />
                        
                        <!--- store last 4 digits of SSN --->
                        <cfset local.xSSN = Right(local.SSN,4) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last 4 digits of SSN: #local.xSSN#</cfoutput><br />
                        
                        <!--- extract member name --->
                        <cfset local.tempMemberLastName = Trim(Mid(local.line,15,20)) />
                        <cfif local.tempMemberLastName NEQ "">
                            <cfset local.memberLastName = local.tempMemberLastName />
                        </cfif>
                        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Last Name: #local.memberLastName#</cfoutput><br />
                        
                        <cfset local.tempMemberFirstName = Trim(Mid(local.line,36,14)) />
                        <cfif local.tempMemberFirstName NEQ "">
                            <cfset local.memberFirstName = local.tempMemberFirstName />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member First Name: #local.memberFirstName#</cfoutput><br />
                        
                        <!--- we have enough information now to do a look up for UIN --->
                        <cfset local.UIN = application.brrDAO.getUIN(local.SSN,local.memberLastName,local.memberFirstName) />
                        <cfoutput>Return value from DAO.getUIN(): #local.UIN#</cfoutput><br />
                        
                        <cfif local.UIN EQ ""><!--- Set UIN to special format in no UINs are found --->
                            <cfset local.UIN = "&" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
                            <cfset local.systemNotes = "No UIN found" />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
                        </cfif>
                        
						<cfif find("UINs found",local.UIN)><!--- Set UIN to special format if more than 1 UIN is found --->
                        	<cfset local.systemNotes = local.UIN />
                            <cfset local.UIN = "*" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UIN: #local.UIN#</cfoutput><br />
                        
                        <!--- extract CFT/PTRY Count --->
                        <cfset local.tempCftCount = Trim(Mid(local.line,55,3)) />
                        <cfif local.tempCftCount NEQ "">
                            <cfset local.cftCount = local.tempCftCount />
                        </cfif>
                        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CFT: #local.cftCount#</cfoutput><br />
                        
                        <cfset local.tempPrtyCount = Trim(Mid(local.line,59,3)) />
                        <cfif local.tempPrtyCount NEQ "">
                            <cfset local.prtyCount = local.tempPrtyCount />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PTRY: #local.prtyCount#</cfoutput><br />
                        
                        <!--- extract Health information --->
                        <cfset local.insHlthCode = Mid(local.line,67,2) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Health Code: #local.insHlthCode#</cfoutput><br />
                        
                        <cfset local.insHlthAmount = Trim(Mid(local.line,69,15)) />
                        <cfif local.insHlthAmount EQ "">
                            <cfset local.insHlthAmount = "0.00" />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Health Amount: #local.insHlthAmount#</cfoutput><br />
                        
                        <!--- extract Dental information --->
                        <cfset local.insLifeCode = Mid(local.line,85,2) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Life Code: #local.insLifeCode#</cfoutput><br />
                        
                        <cfset local.insLifeAmount = Trim(Mid(local.line,87,15)) />
                        <cfif local.insLifeAmount EQ "">
                            <cfset local.insLifeAmount = "0.00" />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Life Amount: #local.insLifeAmount#</cfoutput><br />
                        
                        <!--- extract Dental information --->
                        <cfset local.insDntlCode = Mid(local.line,103,2) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Dental Code: #local.insDntlCode#</cfoutput><br />
                        
                        <cfset local.insDntlAmount = Trim(Mid(local.line,105,15)) />
                        <cfif local.insDntlAmount EQ "">
                            <cfset local.insDntlAmount = "0.00" />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Dental Amount: #local.insDntlAmount#</cfoutput><br />
                        
                        <!--- extract total --->
                        <cfset local.totalAllCarriers = Trim(Mid(local.line,120,12)) />
                        <cfif local.totalAllCarriers EQ "">
                            <cfset local.totalAllCarriers = "0.00" />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Dental Amount: #local.totalAllCarriers#</cfoutput><br />
                        
                        <!--- now that we've read all lines that record should have, we store the information into SQL --->
                        <!--- because of the amount of data, trying to first store these records into structure might fail --->
                        
                        <cfoutput>Attempting to write a record to database.</cfoutput><br />
                    
                        <cfscript>
                            result = application.brrDAO.storePurgeRPTReport(
                                filename=arguments_fileName,
                                reportDate=local.reportDate,
                                reportName="Agency report of discrepant members based on total dollar amount of discrepancy",
                                reportNo=local.reportNo,
                                pageNo=local.pageNo,
                                agencyName=local.agencyName,
                                procOrg=local.procOrg,
                                UIN=local.UIN,
                                xSSN=local.xSSN,
                                memberFirstName=local.memberFirstName,
                                memberLastName=local.memberLastName,
                                cftCount=local.cftCount,
                                prtyCount=local.prtyCount,
                                insHlthCode=local.insHlthCode,
                                insHlthAmount=local.insHlthAmount,
                                insLifeCode=local.insLifeCode,
                                insLifeAmount=local.insLifeAmount,
                                insDntlCode=local.insDntlCode,
                                insDntlAmount=local.insDntlAmount,
                                totalAllCarriers=local.totalAllCarriers,
								systemNotes=local.systemNotes
                            );
                            if(result)
                                WriteOutput("Writing to database done: record saved.<br />");
                            else
                                WriteOutput("Writing to database aborted: record was already found.<br />");
								
							recordCount = recordCount + 1;
                       </cfscript>
                       
                       <!--- after every 5 records, sleep for 1 7/8 seconds (aka 1875 miliseconds), 
					  	  so that we parse a record a max of about 2.33/second (aka 160/minute and 9,600/hour) --->
					   <cfif recordCount MOD 5 EQ 0>
                            <cfthread action="sleep" duration="1875"/>
                       </cfif>
                           
                	</cfif>

                </cfloop><!--- END cfloop[pathName] --->

				<!--- move file --->
				<cfset application.OBFSFileMgmtFacade.makeFilePermanent(tempFileName=arguments_fileName,permanentFileName=arguments_fileName & " " & #DateFormat(Now(),"yy-mm-dd")# & " " & #TimeFormat(Now(),"hh-mm-ss tt")#) />

                <cfcatch type="any">
                	<!--- the controller will catch and process the error --->
                	<cfrethrow />
                </cfcatch>
            </cftry>
            
		</cfthread>

	</cffunction><!--- END readPurgeRPTReport --->
    
</cfcomponent>