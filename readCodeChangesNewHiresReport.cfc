<!--- readCodeChangesNewHiresReportFile.cfc

Purpose: 		this component contains functions that read Code Changes and New Hires Reports
Called by:		brrController.cfc
				brrDAO.cfc
Includes:		none
Modules:		none
CustomTags:		none
Components:		readCodeChangesNewHiresReport	
	
Created:		25 July 2012 by Pawel Czarnota
Last Updated:	$Date: 2014-10-23 14:04:30 -0500 (Thu, 23 Oct 2014) $ by $Author: Pawel Czarnota $ - $Rev: 10427 $

SVN URL:		$URL: https://$svnbase/benefitsreportrepository/trunk/readPurgeReport.cfc $  
Decommissioned: 10/2020
--->

<cfcomponent name="readCodeChangesNewHiresReport" hint="Component to parse Code Changes reports">

	<cffunction name="init" access="public" output="false" hint="Initialize and return readDiscrepancyReport object ">
		
        <cfreturn this />
	</cffunction><!--- END init --->

	<cffunction name="readCodeChangesNewHiresReport" access="public" output="true" returntype="void" hint="reads data from Code Changes Report">
    	<cfargument name="fileName" type="string" required="yes" hint="name of the report file" />
		<cfargument name="pathName" type="string" required="yes" hint="path to the report file" />
        <cfargument name="threadName" type="string" required="yes" hint="name of the thread this will run under" />        
        
        <cfscript>	
			// the processing will be done in a thread
			
			// set up the attributes needed by the thread
			local.attributes = structNew();
			// add all the variables about the upload, the file, and the user
			structInsert(local.attributes, "startTime", now());
			structInsert(local.attributes, "arguments_fileName", arguments.fileName);
			structInsert(local.attributes, "arguments_pathName", arguments.pathName);
			
			//add some application and controller variables the thread will need
			structInsert(local.attributes, "BRRNotificationController", application.BRRNotificationController);
			structInsert(local.attributes, "TechSubject", "Error Processing #arguments.fileName# purge file");
			
			// set the other attributes of the thread 
			structInsert(local.attributes, "threadName", arguments.threadName);
			structInsert(local.attributes, "name", arguments.threadName);
			structInsert(local.attributes, "action", "run");
			// note, the priority is always low
			structInsert(local.attributes, "priority", "Low");
		</cfscript>	
        
        <cfthread attributeCollection="#local.attributes#">
            <cftry>  
            	<cfscript>
            		// this will tell us if there were any problems
					//	 It will be used to tell us if we should or should not send the final success message
					thread.processingError = false;
					thread.fileName = arguments_fileName;
					
					//this variable will be used to make the process sleep
					recordcount = 0;
					
				</cfscript>
        
				<!--- file contains multiple reports, after we read in first line in first report, we set this to false --->
                <cfset local.firstReport = true />
                
                <cfset local.file = FileOpen(arguments_pathName,"read") />
                
                <cfloop condition="NOT FileisEOF(local.file)">
                
					<cfset local.systemNotes = "" />
                
                    <cfset local.line = FileReadLine(local.file) />
                
                    <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput>        
                
                    <!--- 1a. Check for report type and page number --->
                    <cfset local.re = '^?PROGRAM ID:' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <cfset local.reportName = "Summary Report of Benefits Choice Changes" />
                        <cfoutput>Reading Code Changes Report File:  #arguments_fileName#<br /> </cfoutput>
                        
                        <!--- extract page number from line --->
                        <cfset local.pageNo = Trim(Right(local.line,7)) />
                        <!---  account for a case when number is in 1,1234 format --->
                        <cfset local.pageNo = Replace(local.pageNo,",","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page: #local.pageNo#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 1b. Check for report type and page number--->
                    <cfset local.re = '^?PROGRAM ID :' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <cfset local.reportName = "New Members" />
                        <cfoutput>Reading New Hires Report File: #arguments_fileName# <br /> </cfoutput>
                        
                        <!--- extract page number from line --->
                        <cfset local.pageNo = Trim(Right(local.line,7)) />
                        <!---  account for a case when number is in 1,1234 format --->
                        <cfset local.pageNo = Replace(local.pageNo,",","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page: #local.pageNo#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 2. Check for report number --->
                    <cfset local.re = '^REPORT NO.' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        
                        <cfset local.reportNo = Trim(Mid(local.line,14,20)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Number: #local.reportNo#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 3. Check for report date and process date --->
                    <cfset local.re = '^REPORT DATE' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        
                        <cfset local.reportDate = Trim(Mid(local.line,14,10)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Year: #Mid(local.reportDate,7,4)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Day: #Mid(local.reportDate,4,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Month: #Left(local.reportDate,2)#</cfoutput><br />
                        <cfset local.reportDate = CreateDate(Mid(local.reportDate,7,4),Left(local.reportDate,2),Mid(local.reportDate,4,2)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
                        
                        <cfset local.processDate = Trim(Mid(local.line,118,10)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process Date: #local.processDate#</cfoutput><br />
        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;processDate Year: #Mid(local.processDate,7,4)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;processDate Day: #Mid(local.processDate,4,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;processDate Month: #Left(local.processDate,2)#</cfoutput><br />
                        <cfset local.processDate = CreateDate(Mid(local.processDate,7,4),Left(local.processDate,2),Mid(local.processDate,4,2)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Process Date: #local.processDate#</cfoutput><br />
        
                        <cfcontinue />
                    </cfif>
                    
                    <!--- 4a. Check for processing organization and agency name in CodeChanges report --->
                    <cfset local.re = '^.{7}PROC ORG.{3}\d{3}[-]\d{3}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract processing organization # --->
                        <cfset local.procOrg = Mid(local.line,19,11) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proc Org: #local.procOrg#</cfoutput><br />
                        
                        <!--- we need to read in the next file in order to get the agency name --->
                        <cfset local.line = FileReadLine(local.file) />
                        <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput> 
                        
                        <!--- extract agency name --->
                        <cfset local.agencyName = Trim(Mid(local.line,45,Len(local.line))) /><!--- second parameter causes out of bounds but that's okay --->
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agency Name: #local.agencyName#</cfoutput><br />
                        
                        <cfcontinue />
                    </cfif>
                    
                    <!--- 4b. Check for processing organization and agency name in New Hires Report--->
                    <cfset local.re = '^.{7}PROC ORG.{3}\d{3}.\d{3}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract processing organization # --->
                        <cfset local.procOrg = Mid(local.line,19,11) />
                        
                        <!--- add dashes to the number --->
                        <cfset local.procOrg = Replace(local.procOrg," ","-","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proc Org: #local.procOrg#</cfoutput><br />
                        
                        <!--- we need to read in the next file in order to get the agency name --->
                        <cfset local.line = FileReadLine(local.file) />
                        <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput> 
                        
                        <!--- extract agency name --->
                        <cfset local.agencyName = Trim(Mid(local.line,45,Len(local.line))) /><!--- second parameter causes out of bounds but that's okay --->
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agency Name: #local.agencyName#</cfoutput><br />
                        
                        <cfcontinue />
                    </cfif>
                    
                    <!--- 5. Check for SSN, member name, and other record information --->
                    <cfset local.re = '^XXX-XX-\d{4}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract SSN --->
                        <cfset local.SSN = Left(local.line,11) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SSN: #local.SSN#</cfoutput><br />
                        
                        <!--- remove dashes from SSB--->
                        <cfset local.SSN = Replace(local.SSN,"-","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SSN: #local.SSN#</cfoutput><br />
                        
                        <!--- store last 4 digits of SSN --->
                        <cfset local.xSSN = Right(local.SSN,4) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last 4 digits of SSN: #local.xSSN#</cfoutput><br />
                        
                        <!--- extract line 1 of codes (there are 3 lines total) --->
                        <cfscript>
                            local.prevCarrCD1 = Mid(local.line,22,2);
                            local.prevDedCD1 = Mid(local.line,27,2);
                            local.currCarrCD1 = Mid(local.line,32,2);
                            local.currDedCD1 = Mid(local.line,37,2);
                            local.payDate1 = Mid(local.line,41,10);
                            local.payDate1 = CreateDate(Mid(local.payDate1,7,4),Left(local.payDate1,2),Mid(local.payDate1,4,2));
                            local.memberDHAmount1 = Trim(Mid(local.line,53,9));
                            local.depntDHAmount1 = Trim(Mid(local.line,62,10));
                            local.totalDHAmount1 = Trim(Mid(local.line,72,12));
                            local.memberBasicLifeAmount1 = Trim(Mid(local.line,85,7));
                            local.memberOptionalLifeAmount1 = Trim(Mid(local.line,92,9));
                            local.spouseLifeAmount1 = Trim(Mid(local.line,102,6));
                            local.childLifeAmount1 = Trim(Mid(local.line,108,7));
                            local.addLifeAmount1 = Trim(Mid(local.line,115,8));
                            local.totalLifeAmount1 = Trim(Mid(local.line,123,10));
                        </cfscript>      
                        
                        <!--- we need to read in the next file in order to get the member name and line 2 of codes (if they are set); there could be a situation in which a record is broken down into two pages, separated by header. In this case, we need to do some lookahead here --->
                        <cfset local.line = FileReadLine(local.file) />
                        <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput> 
                        <cfif Find("PROGRAM ID",local.line) NEQ 0>
                            <cfset local.line = FileReadLine(local.file) />
                            <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput> 
                            <cfset local.condition = Find("SUBTYPE             CODE CODE CODE CODE DATE",local.line) />
                            <cfloop condition="#local.condition# EQ 0">
                                <cfset local.line = FileReadLine(local.file) />
                                <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput> 
                                <cfset local.condition = Find("SUBTYPE             CODE CODE CODE CODE DATE",local.line) />
                            </cfloop>
                            <cfset local.line = FileReadLine(local.file) />
                            <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput> 
                        </cfif>
                        <!--- extract member name --->
                        <cfset local.memberName = Trim(Left(local.line,20)) /><!--- may cause out of bounds, but that's okay --->
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Name: #local.memberName#</cfoutput><br />
                        
                        <!--- Break down member name into Last name and First name --->
                        <cfscript>
                            local.lastNameEnds = Find(",",local.memberName);
                            local.memberLastName = Trim(Left(local.memberName,local.lastNameEnds-1));
                            local.memberFirstName = Trim(Mid(local.memberName,local.lastNameEnds+1,Len(local.memberName)));/* this causes look up over the lenght of the string but it's okay for our purposes*/
                        </cfscript>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Last Name: #local.memberLastName#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member First Name: #local.memberFirstName#</cfoutput><br />
                        
                        <!--- we have enough information now to do a look up for UIN --->
                        <cfset local.UIN = application.brrDAO.getUIN(local.SSN,local.memberLastName,local.memberFirstName) />
                        <cfoutput>Return value from DAO.getUIN(): #local.UIN#</cfoutput><br />
                        
                        <cfif local.UIN EQ ""><!--- Set UIN to special format in no UINs are found --->
                            <cfset local.UIN = "&" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
                            <cfset local.systemNotes = "No UIN found" />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
                        </cfif>
                        
						<cfif find("UINs found",local.UIN)><!--- Set UIN to special format if more than 1 UIN is found --->
                        	<cfset local.systemNotes = local.UIN />
                            <cfset local.UIN = "*" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UIN: #local.UIN#</cfoutput><br />
                        
                        <cfscript>
                            if(Len(local.line) GT 130){	//extract codes on line 2 including life codes
                                local.prevCarrCD2 = Mid(local.line,22,2);
                                local.prevDedCD2 = Mid(local.line,27,2);
                                local.currCarrCD2 = Mid(local.line,32,2);
                                local.currDedCD2 = Mid(local.line,37,2);
                                local.payDate2 = Mid(local.line,41,10);
                                local.payDate2 = CreateDate(Mid(local.payDate2,7,4),Left(local.payDate2,2),Mid(local.payDate2,4,2));
                                local.memberDHAmount2 = Trim(Mid(local.line,53,9));
                                local.depntDHAmount2 = Trim(Mid(local.line,62,10));
                                local.totalDHAmount2 = Trim(Mid(local.line,72,12));
                                local.memberBasicLifeAmount2 = Trim(Mid(local.line,85,7));
                                local.memberOptionalLifeAmount2 = Trim(Mid(local.line,92,9));
                                local.spouseLifeAmount2 = Trim(Mid(local.line,102,6));
                                local.childLifeAmount2 = Trim(Mid(local.line,108,7));
                                local.addLifeAmount2 = Trim(Mid(local.line,115,8));
                                local.totalLifeAmount2 = Trim(Mid(local.line,123,10));
                            }
                            else if(Len(local.line) GT 21){	//extract codes on line 2 up to Health/Dental codes
                                local.prevCarrCD2 = Mid(local.line,22,2);
                                local.prevDedCD2 = Mid(local.line,27,2);
                                local.currCarrCD2 = Mid(local.line,32,2);
                                local.currDedCD2 = Mid(local.line,37,2);
                                local.payDate2 = Mid(local.line,41,10);
                                local.payDate2 = CreateDate(Mid(local.payDate2,7,4),Left(local.payDate2,2),Mid(local.payDate2,4,2));
                                local.memberDHAmount2 = Trim(Mid(local.line,53,9));
                                local.depntDHAmount2 = Trim(Mid(local.line,62,10));
                                local.totalDHAmount2 = Trim(Mid(local.line,72,12));
                                local.memberBasicLifeAmount2 = "";
                                local.memberOptionalLifeAmount2 = "";
                                local.spouseLifeAmount2 = "";
                                local.childLifeAmount2 = "";
                                local.addLifeAmount2 = "";
                                local.totalLifeAmount2 = "";
                            }
                            else{	//there are no codes, set all to empty string
                                local.prevCarrCD2 = "";
                                local.prevDedCD2 = "";
                                local.currCarrCD2 = "";
                                local.currDedCD2 = "";
                                local.payDate2 = "";
                                local.memberDHAmount2 = "";
                                local.depntDHAmount2 = "";
                                local.totalDHAmount2 = "";
                                local.memberBasicLifeAmount2 = "";
                                local.memberOptionalLifeAmount2 = "";
                                local.spouseLifeAmount2 = "";
                                local.childLifeAmount2 = "";
                                local.addLifeAmount2 = "";
                                local.totalLifeAmount2 = "";
                            }
                        </cfscript>
                        
                        <!--- we need to read in the next file in order to get the employee code and line 3 of codes (if they are set)--->            
                        <cfscript>
                            local.line = FileReadLine(local.file);
                            WriteOutput(local.line & "- Line length:" & Len(local.line) & "<br />");
                            local.empType = Trim(Left(local.line,20)); //may cause out of bounds but that's okay
                            
                            if(Len(local.line) GT 130){	//extract codes on line 2 including life codes
                                local.prevCarrCD3 = Mid(local.line,22,2);
                                local.prevDedCD3 = Mid(local.line,27,2);
                                local.currCarrCD3 = Mid(local.line,32,2);
                                local.currDedCD3 = Mid(local.line,37,2);
                                local.payDate3 = Mid(local.line,41,10);
                                local.payDate3 = CreateDate(Mid(local.payDate3,7,4),Left(local.payDate3,2),Mid(local.payDate3,4,2));
                                local.memberDHAmount3 = Trim(Mid(local.line,53,9));
                                local.depntDHAmount3 = Trim(Mid(local.line,62,10));
                                local.totalDHAmount3 = Trim(Mid(local.line,72,12));
                                local.memberBasicLifeAmount3 = Trim(Mid(local.line,85,7));
                                local.memberOptionalLifeAmount3 = Trim(Mid(local.line,92,9));
                                local.spouseLifeAmount3 = Trim(Mid(local.line,102,6));
                                local.childLifeAmount3 = Trim(Mid(local.line,108,7));
                                local.addLifeAmount3 = Trim(Mid(local.line,115,8));
                                local.totalLifeAmount3 = Trim(Mid(local.line,123,10));
                            }
                            else if(Len(local.line) GT 21){	//extract codes on line 2 up to Health/Dental codes
                                local.prevCarrCD3 = Mid(local.line,22,2);
                                local.prevDedCD3 = Mid(local.line,27,2);
                                local.currCarrCD3 = Mid(local.line,32,2);
                                local.currDedCD3 = Mid(local.line,37,2);
                                local.payDate3 = Mid(local.line,41,10);
                                local.payDate3 = CreateDate(Mid(local.payDate3,7,4),Left(local.payDate3,2),Mid(local.payDate3,4,2));
                                local.memberDHAmount3 = Trim(Mid(local.line,53,9));
                                local.depntDHAmount3 = Trim(Mid(local.line,62,10));
                                local.totalDHAmount3 = Trim(Mid(local.line,72,12));
                                local.memberBasicLifeAmount3 = "";
                                local.memberOptionalLifeAmount3 = "";
                                local.spouseLifeAmount3 = "";
                                local.childLifeAmount3 = "";
                                local.addLifeAmount3 = "";
                                local.totalLifeAmount3 = "";
                            }
                            else{	//there are no codes, set all to empty string
                                local.prevCarrCD3 = "";
                                local.prevDedCD3 = "";
                                local.currCarrCD3 = "";
                                local.currDedCD3 = "";
                                local.payDate3 = "";
                                local.memberDHAmount3 = "";
                                local.depntDHAmount3 = "";
                                local.totalDHAmount3 = "";
                                local.memberBasicLifeAmount3 = "";
                                local.memberOptionalLifeAmount3 = "";
                                local.spouseLifeAmount3 = "";
                                local.childLifeAmount3 = "";
                                local.addLifeAmount3 = "";
                                local.totalLifeAmount3 = "";
                            }
                        </cfscript>
                        
                        <!--- we need to read in the next file in order to get the Full Time/Part Time status--->
                        <cfset local.line = FileReadLine(local.file) />
                        <cfset local.FtptStatus = Trim(local.line) />
                        
                        <!--- now that we've read all lines that record should have, we store the information into the SQL database. Trying to store this info to structure first may fail due to amount of data --->
                        
                        <cfif local.reportName EQ "New Members">
                            <cfscript>
                                result = application.brrDAO.storeCodeChangesNewHiresReport(
                                    filename = arguments_fileName,
                                    reportDate = local.reportDate,
                                    reportName = local.reportName,
                                    reportNo = local.reportNo,
                                    pageNo = local.pageNo,
                                    agencyName = local.agencyName,
                                    procOrg = local.procOrg,
                                    UIN = local.UIN,
                                    xSSN = local.xSSN,
                                    memberFirstName = local.memberFirstName,
                                    memberLastName = local.memberLastName,
                                    processDate = local.processDate,
                                    empType = local.empType,
                                    FtptStatus = local.FtptStatus,
                                    currCarrCD1 = local.currCarrCD1,
                                    currDedCD1 = local.currDedCD1,
                                    payDate1 = local.payDate1,
                                    memberDHAmount1 = local.memberDHAmount1,
                                    depntDHAmount1 = local.depntDHAmount1,
                                    totalDHAmount1 = local.totalDHAmount1,
                                    memberBasicLifeAmount1 = local.memberBasicLifeAmount1,
                                    memberOptionalLifeAmount1 = local.memberOptionalLifeAmount1,
                                    spouseLifeAmount1 = local.spouseLifeAmount1,
                                    childLifeAmount1 = local.childLifeAmount1,
                                    addLifeAmount1 = local.addLifeAmount1,
                                    totalLifeAmount1 = local.totalLifeAmount1,
                                    currCarrCD2 = local.currCarrCD2,
                                    currDedCD2 = local.currDedCD2,
                                    payDate2 = local.payDate2,
                                    memberDHAmount2 = local.memberDHAmount2,
                                    depntDHAmount2 = local.depntDHAmount2,
                                    totalDHAmount2 = local.totalDHAmount2,
                                    memberBasicLifeAmount2 = local.memberBasicLifeAmount2,
                                    memberOptionalLifeAmount2 = local.memberOptionalLifeAmount2,
                                    spouseLifeAmount2 = local.spouseLifeAmount2,
                                    childLifeAmount2 = local.childLifeAmount2,
                                    addLifeAmount2 = local.addLifeAmount2,
                                    totalLifeAmount2 = local.totalLifeAmount2,
                                    currCarrCD3 = local.currCarrCD3,
                                    currDedCD3 = local.currDedCD3,
                                    payDate3 = local.payDate3,
                                    memberDHAmount3 = local.memberDHAmount3,
                                    depntDHAmount3 = local.depntDHAmount3,
                                    totalDHAmount3 = local.totalDHAmount3,
                                    memberBasicLifeAmount3 = local.memberBasicLifeAmount3,
                                    memberOptionalLifeAmount3 = local.memberOptionalLifeAmount3,
                                    spouseLifeAmount3 = local.spouseLifeAmount3,
                                    childLifeAmount3 = local.childLifeAmount3,
                                    addLifeAmount3 = local.addLifeAmount3,
                                    totalLifeAmount3 = local.totalLifeAmount3,
									systemNotes = local.systemNotes
                                );
                                if(result)
                                    WriteOutput("Writing to database done: record saved.<br />");
                                else
                                    WriteOutput("Writing to database aborted: record was already found.<br />");
                           </cfscript>
                       <cfelse><!--- CodeChanges report --->
                            <cfscript>
                                result = application.brrDAO.storeCodeChangesNewHiresReport(
                                    filename = arguments_fileName,
                                    reportDate = local.reportDate,
                                    reportName = local.reportName,
                                    reportNo = local.reportNo,
                                    pageNo = local.pageNo,
                                    agencyName = local.agencyName,
                                    procOrg = local.procOrg,
                                    UIN = local.UIN,
                                    xSSN = local.xSSN,
                                    memberFirstName = local.memberFirstName,
                                    memberLastName = local.memberLastName,
                                    processDate = local.processDate,
                                    empType = local.empType,
                                    FtptStatus = local.FtptStatus,
                                    prevCarrCD1 = local.prevCarrCD1,
                                    currCarrCD1 = local.currCarrCD1,
                                    prevDedCD1 = local.prevDedCD1,
                                    currDedCD1 = local.currDedCD1,
                                    payDate1 = local.payDate1,
                                    memberDHAmount1 = local.memberDHAmount1,
                                    depntDHAmount1 = local.depntDHAmount1,
                                    totalDHAmount1 = local.totalDHAmount1,
                                    memberBasicLifeAmount1 = local.memberBasicLifeAmount1,
                                    memberOptionalLifeAmount1 = local.memberOptionalLifeAmount1,
                                    spouseLifeAmount1 = local.spouseLifeAmount1,
                                    childLifeAmount1 = local.childLifeAmount1,
                                    addLifeAmount1 = local.addLifeAmount1,
                                    totalLifeAmount1 = local.totalLifeAmount1,
                                    prevCarrCD2 = local.prevCarrCD2,
                                    currCarrCD2 = local.currCarrCD2,
                                    prevDedCD2 = local.prevDedCD2,
                                    currDedCD2 = local.currDedCD2,
                                    payDate2 = local.payDate2,
                                    memberDHAmount2 = local.memberDHAmount2,
                                    depntDHAmount2 = local.depntDHAmount2,
                                    totalDHAmount2 = local.totalDHAmount2,
                                    memberBasicLifeAmount2 = local.memberBasicLifeAmount2,
                                    memberOptionalLifeAmount2 = local.memberOptionalLifeAmount2,
                                    spouseLifeAmount2 = local.spouseLifeAmount2,
                                    childLifeAmount2 = local.childLifeAmount2,
                                    addLifeAmount2 = local.addLifeAmount2,
                                    totalLifeAmount2 = local.totalLifeAmount2,
                                    prevCarrCD3 = local.prevCarrCD3,
                                    currCarrCD3 = local.currCarrCD3,
                                    prevDedCD3 = local.prevDedCD3,
                                    currDedCD3 = local.currDedCD3,
                                    payDate3 = local.payDate3,
                                    memberDHAmount3 = local.memberDHAmount3,
                                    depntDHAmount3 = local.depntDHAmount3,
                                    totalDHAmount3 = local.totalDHAmount3,
                                    memberBasicLifeAmount3 = local.memberBasicLifeAmount3,
                                    memberOptionalLifeAmount3 = local.memberOptionalLifeAmount3,
                                    spouseLifeAmount3 = local.spouseLifeAmount3,
                                    childLifeAmount3 = local.childLifeAmount3,
                                    addLifeAmount3 = local.addLifeAmount3,
                                    totalLifeAmount3 = local.totalLifeAmount3,
									systemNotes = local.systemNotes
                                );
                                if(result)
                                    WriteOutput("Writing to database done: record saved.<br />");
                                else
                                    WriteOutput("Writing to database aborted: record was already found.<br />");
                           </cfscript>
                           
                       </cfif><!--- END cfif[reportName EQ "New Members"] --->
                       
                       <cfset recordCount = recordCount + 1 />
                       
                       <!--- after every 5 records, sleep for 1 7/8 seconds (aka 1875 miliseconds), 
					  	  so that we parse a record a max of about 2.33/second (aka 160/minute and 9,600/hour) --->
					   <cfif recordCount MOD 5 EQ 0>
                            <cfthread action="sleep" duration="1875"/>
                       </cfif>
                            
                   </cfif><!--- END cfif(5.) --->
                    
                </cfloop><!--- END cfloop[readFile] --->
                
                <cfset FileClose(local.file) />
                
                <!--- move file --->
				<cfset application.OBFSFileMgmtFacade.makeFilePermanent(tempFileName=arguments_fileName,permanentFileName=arguments_fileName & " " & #DateFormat(Now(),"yy-mm-dd")# & " " & #TimeFormat(Now(),"hh-mm-ss tt")#) />

                <cfcatch type="any">
                	<!--- the controller will catch and process the error --->
                	<cfrethrow />
                </cfcatch>
            </cftry>
            
		</cfthread>
        
	</cffunction><!--- END readCodeChangesNewHiresReport --->
    
</cfcomponent>