<!--- readDiscrepancyDetailReport.cfc

Purpose:		this component contains functions that read Discrepancy Detail Reports
Called by:		brrController.cfc
				brrDAO.cfc
Includes:		none
Modules:		none
CustomTags:		none
Components:		readDiscrepancyReport

Created:		21 June 2012 by Pawel Czarnota
Last Updated:	$Date: 2014-10-23 14:04:30 -0500 (Thu, 23 Oct 2014) $ by $Author: Pawel Czarnota $ - $Rev: 10427 $

SVN URL:		$URL: https://$svnbase/benefitsreportrepository/trunk/readPurgeReport.cfc $  
Decommissioned: 10/2020
--->
--->
<cfcomponent name="readDiscrepancyReport" hint="Component to parse discrepancy detail reports">

	<cffunction name="init" access="public" output="false" hint="Initialize and return readDiscrepancyReport object ">
		
        <cfreturn this />
	</cffunction><!--- END init --->

	<cffunction name="readDiscrepancyReport" access="public" output="true" returntype="void" hint="reads data from discrepancy detail report">
    	<cfargument name="fileName" type="string" required="yes" hint="name of the report file" />
		<cfargument name="pathName" type="string" required="yes" hint="path to the report file" />
        <cfargument name="threadName" type="string" required="yes" hint="name of the thread this will run under" />
        
        <cfoutput>Reading Discrepancy Detail Report File: #arguments.fileName# <br /> </cfoutput>
        
        <cfscript>	
			// the processing will be done in a thread
			
			// set up the attributes needed by the thread
			local.attributes = structNew();
			// add all the variables about the upload, the file, and the user
			structInsert(local.attributes, "startTime", now());
			structInsert(local.attributes, "arguments_fileName", arguments.fileName);
			structInsert(local.attributes, "arguments_pathName", arguments.pathName);
			
			//add some application and controller variables the thread will need
			structInsert(local.attributes, "BRRNotificationController", application.BRRNotificationController);
			structInsert(local.attributes, "TechSubject", "Error Processing #arguments.fileName# purge file");
			
			// set the other attributes of the thread 
			structInsert(local.attributes, "threadName", arguments.threadName);
			structInsert(local.attributes, "name", arguments.threadName);
			structInsert(local.attributes, "action", "run");
			// note, the priority is always low
			structInsert(local.attributes, "priority", "Low");
		</cfscript>	
        
        <cfthread attributeCollection="#local.attributes#">
            <cftry>  
            	<cfscript>
            		// this will tell us if there were any problems
					//	 It will be used to tell us if we should or should not send the final success message
					thread.processingError = false;
					thread.fileName = arguments_fileName;
					
					//this variable will be used to make the process sleep
					recordcount = 0;
					
				</cfscript>
        
				<!--- Store each record in data structure --->
                <cfset local.records = ArrayNew(1) />
            
                <!--- index to help us set up records data structure --->
                <cfset local.recordNo = 1 /> 
                
                <!--- file contains multiple reports, after we read in first line in first report, we set this to false --->
                <cfset local.firstReport = true />
                
                <cfloop file="#arguments_pathName#" index="local.line">
                    <cfoutput>#line# - Line length: #Len(local.line)#<br /></cfoutput>        
                
                	<cfset local.systemNotes = "" />
                
                    <!--- 1. Check for report date in line--->
                    <cfset local.re = '^?\d{2}[/]\d{2}[/]\d{2}.{33}ILLINOIS' />
                    <cfset local.match = REFind(local.re,local.line)>
                    <cfif local.match neq 0>
                        <!--- extract report date from line --->
                        <cfif firstReport eq true>
                            <cfset local.reportDate = Left(local.line,8) />
                            <cfset local.firstReport = false />
                        <cfelse>
                            <cfset local.reportDate = Left(local.line,9) />
                            <cfset local.reportDate = Right(local.reportDate,8) />
                        </cfif>
        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Year: #"20" & Mid(local.reportDate,7,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Day: #Mid(local.reportDate,4,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Month: #Left(local.reportDate,2)#</cfoutput><br />
                        <cfset local.reportDate = CreateDate("20" & Mid(local.reportDate,7,2),Left(local.reportDate,2),Mid(local.reportDate,4,2)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
                        
                        <!--- extract page number from line --->
                        <cfset local.pageNo = Trim(Right(local.line,7)) /><br>
						<!---  account for a case when number is in 1,1234 format --->
                        <cfset local.pageNo = Replace(local.pageNo,",","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page: #local.pageNo#</cfoutput><br />
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 2. Record report number and detailed report name --->
                    <cfset local.re = '^PROGRAM.' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        
                        <cfset local.reportName = Left(local.line,109) />
                        <cfset local.reportName = Trim(Right(local.reportName,91)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Name: #local.reportName#</cfoutput><br />
                        
                        <cfset local.reportNo = Left(local.line,132) />
                        <cfset local.reportNo = Trim(Right(local.reportNo,12)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Number: #local.reportNo#</cfoutput><br />
        
                        <cfcontinue />
                    </cfif>
                    
                    <!--- 3. Check for agency name --->
                    <cfset local.re = '^.{46}AGENCY:.' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract agency name --->
                        <cfset local.agencyName = Trim(Mid(local.line,56,Len(local.line))) /><!--- second parameter causes out of bounds but that's okay --->
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agency Name: #local.agencyName#</cfoutput><br />
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 4. Check for line ONE items --->
                    <cfset local.re = '^.\d{2}[-]\d{3}.\d{2}[/]\d{2}[/]\d{2}.' />
                    <cfset local.match = REFind(local.re,line) />
                    <cfif local.match neq 0>
                        <!--- extract line ONE items --->
                        <cfset local.payCode = Left(local.line,7) />
                        <cfset local.payCode = Trim(Right(local.payCode,6)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Code: #local.payCode#</cfoutput><br />
                        
                        <cfset local.payPeriod = Left(line,16) />
                        <cfset local.payPeriod = Trim(Right(local.payPeriod,8)) />
                        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period Ending: #local.payPeriod#</cfoutput><br />		
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period Ending Year: #"20" & Mid(payPeriod,7,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period Ending Day: #Mid(local.payPeriod,4,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period Ending Month: #Left(local.payPeriod,2)#</cfoutput><br />
                        <cfset local.payPeriod = CreateDate("20" & Mid(local.payPeriod,7,2),Left(local.payPeriod,2),Mid(local.payPeriod,4,2)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period Ending: #local.payPeriod#</cfoutput><br />
                        
                        <cfset local.SSN = Left(local.line,30) />
                        <cfset local.SSN = Right(local.SSN,11) />
                        <!--- remove dashes from SSB--->
                        <cfset local.SSN = Replace(local.SSN,"-","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SSN: #local.SSN#</cfoutput><br />
                        
                        <!--- store last 4 digits of SSN --->
                        <cfset local.xSSN = Right(local.SSN,4) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last 4 digits of SSN: #local.xSSN#</cfoutput><br />
                        
                        <cfset local.insRecType = Left(local.line,57) />
                        <cfset local.insRecType = Trim(Right(local.insRecType,5)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Rec Type: #local.insRecType#</cfoutput><br />
                        
                        <cfset local.type = Left(local.line,64) />
                        <cfset local.type = Trim(Right(local.type,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type: #local.type#</cfoutput><br />
                        
                        <cfset local.insHlthCarr = Left(local.line,71) />
                        <cfset local.insHlthCarr = Trim(Right(local.insHlthCarr,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Health Carrier: #local.insHlthCarr#</cfoutput><br />
                        
                        <cfset local.insHlthDed = Left(local.line,78) />
                        <cfset local.insHlthDed = Trim(Right(local.insHlthDed,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Health Deduction: #local.insHlthDed#</cfoutput><br />
                        
                        <cfset local.insDntlCarr = Left(local.line,85) />
                        <cfset local.insDntlCarr = Trim(Right(local.insDntlCarr,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Dental Carrier: #local.insDntlCarr#</cfoutput><br />
                        
                        <cfset local.insDntlDed = Left(local.line,92) />
                        <cfset local.insDntlDed = Trim(Right(local.insDntlDed,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Dental Deduction: #local.insDntlDed#</cfoutput><br />
                        
                        <cfset local.insLifeCarr = Left(local.line,99) />
                        <cfset local.insLifeCarr = Trim(Right(local.insLifeCarr,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Life Carrier: #local.insLifeCarr#</cfoutput><br />
                        
                        <cfset local.insLifeDed = Left(local.line,105) />
                        <cfset local.insLifeDed = Trim(Right(local.insLifeDed,6)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Life Deduction: #local.insLifeDed#</cfoutput><br />
                        
                        <cfset local.insLifeUnits = Left(local.line,113) />
                        <cfset local.insLifeUnits = Trim(Right(local.insLifeUnits,8)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Life Units: #local.insLifeUnits#</cfoutput><br />
                        
                        <cfset local.insFtptPerc = Left(local.line,122) />
                        <cfset local.insFtptPerc = Trim(Right(local.insFtptPerc,9)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins FTPT Percentage: #local.insFtptPerc#</cfoutput><br />
                        
                        <cfset local.insBirthDate = Left(local.line,130) />
                        <cfset local.insBirthDate = Right(local.insBirthDate,8) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ins Birth Date: #local.insBirthDate#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Birth Year: #"19" & Mid(local.insBirthDate,7,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Birth Day: #Mid(local.insBirthDate,4,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Birth Month: #Left(local.insBirthDate,2)#</cfoutput><br />
                        <cfset local.birthDate = CreateDate("19" & Mid(local.insBirthDate,7,2),Left(local.insBirthDate,2),Mid(local.insBirthDate,4,2)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Birth Date: #local.birthDate#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 5. Check for line TWO items --->
                    <cfset local.re = '^.\d{3}[-]\d{3}[-]\d{3}.{7}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract line TWO items --->
                        <cfset local.procOrg = Mid(local.line,2,11) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proc Org: #local.procOrg#</cfoutput><br />
                                    
                        <cfset local.memberName = Left(local.line,52) />
                        <cfset local.memberName = Trim(Right(local.memberName,33)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Name: #local.memberName#</cfoutput><br />
                        
                        <!--- Break down member name into Last name, First name and MI --->
                        <cfscript>
                            local.lastNameEnds = Find(", ",local.memberName);
                            if (local.lastNameEnds EQ 0)
                                local.lastNameEnds = Find(" ",local.memberName);	//in rare cases, comma might be missing from the memberName in the report file, in this case we try to use space as separator
                            local.memberLastName = Left(local.memberName,local.lastNameEnds-1);
                            local.firstNameAndMI = Mid(local.memberName,local.lastNameEnds+1,Len(local.memberName));/* this causes look up over the lenght of the string but it's okay for our purposes*/
                            local.firstNameAndMI = Trim(local.firstNameAndMI);
                            local.firstNameAndMIReversed = Reverse(local.firstNameAndMI);
                            local.firstNameEnds = Find(" ",local.firstNameAndMIReversed);
                            if(local.firstNameEnds eq 2){
                                local.memberMI = Left(local.FirstNameAndMIReversed,1);	
                                local.memberFirstName = Reverse(Mid(local.firstNameAndMIReversed,3,Len(local.firstNameAndMI)));/* this causes look up over the lenghts of the string but it's okay for our purposes*/
                            }
                            else{
                                local.memberMI = "";
                                local.memberFirstName = Reverse(local.firstNameAndMIReversed);
                            }
                        </cfscript>                        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Last Name: #local.memberLastName#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member First Name: #local.memberFirstName#</cfoutput><br />	
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member MI: #local.memberMI#</cfoutput><br />                   
                        
                        <!--- we have enough information now to do a look up for UIN --->
                        <cfset local.UIN = application.brrDAO.getUIN(local.SSN,local.memberLastName,local.memberFirstName) />
                        <cfoutput>Return value from DAO.getUIN(): #local.UIN#</cfoutput><br />
                        
                        <cfif local.UIN EQ ""><!--- Set UIN to special format in no UINs are found --->
                            <cfset local.UIN = "&" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
                            <cfset local.systemNotes = "No UIN found" />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
                        </cfif>
                        
						<cfif find("UINs found",local.UIN)><!--- Set UIN to special format if more than 1 UIN is found --->
                        	<cfset local.systemNotes = local.UIN />
                            <cfset local.UIN = "*" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UIN: #local.UIN#</cfoutput><br />
                        
                        <cfset local.payRecType = Left(local.line,57) />
                        <cfset local.payRecType = Trim(Right(local.payRecType,5)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Rec Type: #local.payRecType#</cfoutput><br />
                        
                        <cfset local.subtype = Left(local.line,64) />
                        <cfset local.subtype = Trim(Right(local.subtype,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtype: #local.subtype#</cfoutput><br />
                        
                        <cfset local.payHlthCarr = Left(local.line,71) />
                        <cfset local.payHlthCarr = Trim(Right(local.payHlthCarr,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Health Carrier: #local.payHlthCarr#</cfoutput><br />
                        
                        <cfset local.payHlthDed = Left(local.line,78) />
                        <cfset local.payHlthDed = Trim(Right(local.payHlthDed,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Health Deduction: #local.payHlthDed#</cfoutput><br />
                        
                        <cfset local.payDntlCarr = Left(local.line,85) />
                        <cfset local.payDntlCarr = Trim(Right(local.payDntlCarr,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Dental Carrier: #local.payDntlCarr#</cfoutput><br />
                        
                        <cfset local.payDntlDed = Left(local.line,92) />
                        <cfset local.payDntlDed = Trim(Right(local.payDntlDed,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Dental Deduction: #local.payDntlDed#</cfoutput><br />
                        
                        <cfset local.payLifeCarr = Left(local.line,99) />
                        <cfset local.payLifeCarr = Trim(Right(local.payLifeCarr,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Life Carrier: #local.payLifeCarr#</cfoutput><br />
                        
                        <cfset local.payLifeDed = Left(local.line,105) />
                        <cfset local.payLifeDed = Trim(Right(local.payLifeDed,6)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Life Deduction: #local.payLifeDed#</cfoutput><br />
                        
                        <cfset local.payLifeUnits = Left(local.line,113) />
                        <cfset local.payLifeUnits = Trim(Right(local.payLifeUnits,8)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Life Units: #local.payLifeUnits#</cfoutput><br />
                        
                        <cfset local.payFtptPerc = Left(local.line,122) />
                        <cfset local.payFtptPerc = Trim(Right(local.payFtptPerc,9)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay FTPT Percentage: #local.payFtptPerc#</cfoutput><br />
                        
                        <!--- we don't need to read pay birth date, it's the same as ins birth date
                        <cfset local.payBirthDate = Left(local.line,130) />
                        <cfset local.payBirthDate = Right(local.payBirthDate,8) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Birth Date: local.payBirthDate</cfoutput><br />
                        --->
                    
                        <!--- now that we've read all lines that record should have, we store the information into structure --->
                        <cfset local.records[local.recordNo] = StructNew() />
                        <cfset local.records[local.recordNo].fileName = arguments_fileName />
                        <cfset local.records[local.recordNo].reportDate = local.reportDate />
                        <cfset local.records[local.recordNo].pageNo = local.pageNo />
                        <cfset local.records[local.recordNo].reportName = local.reportName />
                        <cfset local.records[local.recordNo].reportNo = local.reportNo />
                        <cfset local.records[local.recordNo].agencyName = local.agencyName />
                        <cfset local.records[local.recordNo].payCode = local.payCode />
                        <cfset local.records[local.recordNo].payPeriod = local.payPeriod />
                        <cfset local.records[local.recordNo].UIN = local.UIN />
                        <cfset local.records[local.recordNo].xSSN = local.xSSN />
                        <cfset local.records[local.recordNo].insRecType = local.insRecType />
                        <cfset local.records[local.recordNo].type = local.type />
                        <cfset local.records[local.recordNo].insHlthCarr = local.insHlthCarr />
                        <cfset local.records[local.recordNo].insHlthDed = local.insHlthDed />
                        <cfset local.records[local.recordNo].insDntlCarr = local.insDntlCarr />
                        <cfset local.records[local.recordNo].insDntlDed = local.insDntlDed />
                        <cfset local.records[local.recordNo].insLifeCarr = local.insLifeCarr />
                        <cfset local.records[local.recordNo].insLifeDed = local.insLifeDed />
                        <cfset local.records[local.recordNo].insLifeUnits = local.insLifeUnits />
                        <cfset local.records[local.recordNo].insFtptPerc = local.insFtptPerc />
                        <cfset local.records[local.recordNo].birthDate = local.insBirthDate />
                        <cfset local.records[local.recordNo].procOrg = local.procOrg />
                        <cfset local.records[local.recordNo].memberLastName = local.memberLastName />
                        <cfset local.records[local.recordNo].memberFirstName = local.memberFirstName />
                        <cfset local.records[local.recordNo].memberMI = local.memberMI />
                        <cfset local.records[local.recordNo].payRecType = local.payRecType />
                        <cfset local.records[local.recordNo].subtype = local.subtype />
                        <cfset local.records[local.recordNo].payHlthCarr = local.payHlthCarr />
                        <cfset local.records[local.recordNo].payHlthDed = local.payHlthDed />
                        <cfset local.records[local.recordNo].payDntlCarr = local.payDntlCarr />
                        <cfset local.records[local.recordNo].payDntlDed = local.payDntlDed />
                        <cfset local.records[local.recordNo].payLifeCarr = local.payLifeCarr />
                        <cfset local.records[local.recordNo].payLifeDed = local.payLifeDed />
                        <cfset local.records[local.recordNo].payLifeUnits = local.payLifeUnits />
                        <cfset local.records[local.recordNo].payFtptPerc = local.payFtptPerc />
                        <cfset local.records[local.recordNo].specialNote = "" />
                        <cfset local.records[local.recordNo].systemNotes = local.systemNotes />
                        <!--- we need to increment record number to be ready for next record --->
                        <cfset local.recordNo = local.recordNo + 1 />
                        
                        <cfcontinue />
                    </cfif>
                    
                    <!--- 6. Last thing to check for is special note about membership liability --->
                    <cfset local.re = '\* THERE IS NO MEMBERSHIP LIABILITY FOR THIS PAY PERIOD \*' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- add this note to the last record --->
                        <cfset local.records[local.recordNo-1].specialNote = "* THERE IS NO MEMBERSHIP LIABILITY FOR THIS PAY PERIOD *" />
                        <cfoutput>Special Note: #local.records[local.recordNo-1].specialNote#</cfoutput>
                        <cfcontinue />
                    </cfif>
                </cfloop><!--- END cfloop[pathName] --->
                
                Here is a list of all records read in from the <cfoutput>#arguments_fileName#</cfoutput> file:
                <cfdump var="#local.records#" />
                
                <!--- add these records into database, if they already exist, update them --->
                <cfloop index="local.i" from="1" to="#ArrayLen(local.records)#">
                    <cfoutput>Attempting to write a record to database.</cfoutput><br />
        
                        <cfset result = application.brrDAO.storeDiscepancyReport(
                         filename=local.records[local.i].fileName,
                         reportDate=local.records[local.i].reportDate,
                         reportName=local.records[local.i].reportName,
                         reportNo=local.records[local.i].reportNo,
                         pageNo=local.records[local.i].pageNo,
                         agencyName=local.records[local.i].agencyName,
                         procOrg=local.records[local.i].procOrg,
                         UIN=local.records[local.i].UIN,
                         xSSN=local.records[local.i].xSSN,
                         memberFirstName=local.records[local.i].memberFirstName,
                         memberLastName=local.records[local.i].memberLastName,
                         memberMI=local.records[local.i].memberMI,
                         birthDate=local.records[local.i].birthDate,
                         payCode=local.records[local.i].payCode,
                         payPeriod=local.records[local.i].payPeriod,
                         insRecType=local.records[local.i].insRecType,
                         type=local.records[local.i].type,
                         insHlthCarr=local.records[local.i].insHlthCarr,
                         insHlthDed=local.records[local.i].insHlthDed,
                         insDntlCarr=local.records[local.i].insDntlCarr,
                         insDntlDed=local.records[local.i].insDntlDed,
                         insLifeCarr=local.records[local.i].insLifeCarr,
                         insLifeDed=local.records[local.i].insLifeDed,
                         insLifeUnits=local.records[local.i].insLifeUnits,
                         insFtptPerc=local.records[local.i].insFtptPerc,
                         payRecType=local.records[local.i].payRecType,
                         subtype=local.records[local.i].subtype,
                         payHlthCarr=local.records[local.i].payHlthCarr,
                         payHlthDed=local.records[local.i].payHlthDed,
                         payDntlCarr=local.records[local.i].payDntlCarr,
                         payDntlDed=local.records[local.i].payDntlDed,
                         payLifeCarr=local.records[local.i].payLifeCarr,
                         payLifeDed=local.records[local.i].payLifeDed,
                         payLifeUnits=local.records[local.i].payLifeUnits,
                         payFtptPerc=local.records[local.i].payFtptPerc,
                         specialNote=local.records[local.i].specialNote,
						 systemNotes=local.records[local.i].systemNotes
                        ) />
                        <cfscript>
                            if(result)
                                WriteOutput("Writing to database done: record saved.<br />");
                            else
                                WriteOutput("Writing to database aborted: record was already found.<br />");
                        </cfscript>
                        
                        <cfset recordCount = recordCount + 1 />
                       
                       <!--- after every 5 records, sleep for 1 7/8 seconds (aka 1875 miliseconds), 
					  	  so that we parse a record a max of about 2.33/second (aka 160/minute and 9,600/hour) --->
					   <cfif recordCount MOD 5 EQ 0>
                            <cfthread action="sleep" duration="1875"/>
                       </cfif>
                        
                </cfloop><!--- END cfloop[records] --->
                
                <!--- move file --->
				<cfset application.OBFSFileMgmtFacade.makeFilePermanent(tempFileName=arguments_fileName,permanentFileName=arguments_fileName & " " & #DateFormat(Now(),"yy-mm-dd")# & " " & #TimeFormat(Now(),"hh-mm-ss tt")#) />

                <cfcatch type="any">
                	<!--- the controller will catch and process the error --->
                	<cfrethrow />
                </cfcatch>
            </cftry>
            
		</cfthread>
        
	</cffunction><!--- END readDiscrepancyReport --->
    
</cfcomponent>