<!--- readSixMonthsRPTReportFile.cfc

Purpose: 		this component contains functions that read Six Months RPT Reports
Called by:		brrController.cfc
				brrDAO.cfc
Includes:		none
Modules:		none
CustomTags:		none
Components:		readSixMonthsRPTReport	
	
Created:		21 June 2012 by Pawel Czarnota
Last Updated:	$Date: 2014-10-23 14:04:30 -0500 (Thu, 23 Oct 2014) $ by $Author: Pawel Czarnota $ - $Rev: 10427 $

SVN URL:		$URL: https://$svnbase/benefitsreportrepository/trunk/readSixMonthsReport.cfc $  
Decommissioned: 10/2020
--->

<cfcomponent name="readSixMonthsReport" hint="Component to parse SixMonths reports">

	<cffunction name="init" access="public" output="false" hint="Initialize and return readDiscrepancyReport object ">
		
        <cfreturn this />
	</cffunction><!--- END init --->

	<cffunction name="readSixMonthsReport" access="public" output="true" returntype="void" hint="reads data from Six Months RPT Report">
    	<cfargument name="fileName" type="string" required="yes" hint="name of the report file" />
		<cfargument name="pathName" type="string" required="yes" hint="path to the report file" />
        <cfargument name="threadName" type="string" required="yes" hint="name of the thread this will run under" />
        
        <cfoutput>Reading Six Months Report File: #arguments.fileName# <br /> </cfoutput>
        
        <cfscript>	
			// the processing will be done in a thread
			
			// set up the attributes needed by the thread
			local.attributes = structNew();
			// add all the variables about the upload, the file, and the user
			structInsert(local.attributes, "startTime", now());
			structInsert(local.attributes, "arguments_fileName", arguments.fileName);
			structInsert(local.attributes, "arguments_pathName", arguments.pathName);
			
			//add some application and controller variables the thread will need
			structInsert(local.attributes, "BRRNotificationController", application.BRRNotificationController);
			structInsert(local.attributes, "TechSubject", "Error Processing #arguments.fileName# purge file");
			
			// set the other attributes of the thread 
			structInsert(local.attributes, "threadName", arguments.threadName);
			structInsert(local.attributes, "name", arguments.threadName);
			structInsert(local.attributes, "action", "run");
			// note, the priority is always low
			structInsert(local.attributes, "priority", "Low");
		</cfscript>	
        
       <cfthread attributeCollection="#local.attributes#">
            <cftry>  
            	<cfscript>
            		// this will tell us if there were any problems
					//	 It will be used to tell us if we should or should not send the final success message
					thread.processingError = false;
					thread.fileName = arguments_fileName;
					
					//this variable will be used to make the process sleep
					recordcount = 0;
					
				</cfscript>
                
                <!--- file contains multiple reports, after we read in first line in first report, we set this to false --->
        		<cfset local.firstReport = true />
        
                <cfloop file="#arguments_pathName#" index="local.line">
                    <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput>        
                
                	<cfset local.systemNotes = "" />
                
                    <!--- 1. Check for report number--->
                    <cfset local.re = '^?REPORT:.{33}ILLINOIS' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                    
                        <!--- extract report number from line --->
                        <cfif local.firstReport eq true>
                            <cfset local.reportNo = Left(local.line,25) />
                            <cfset local.reportNo = Trim(Right(local.reportNo,16)) />
                            <cfset local.firstReport = false />
                        <cfelse>
                            <cfset local.reportNo = Left(local.line,25) />
                            <cfset local.reportNo = Trim(Right(local.reportNo,15)) />
                        </cfif>
                        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Number: #local.reportNo#</cfoutput><br />
                        
                        <!--- extract page number from line --->
                        <cfset local.pageNo = Trim(Right(local.line,7)) />
                        <!---  account for a case when number is in 1,1234 format --->
                        <cfset local.pageNo = Replace(local.pageNo,",","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page: #local.pageNo#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 2. Check for report date --->
                    <cfset local.re = '^PROGRAM:.' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        
                        <cfset local.reportDate = Left(local.line,131) />
                        <cfset local.reportDate = Trim(Right(local.reportDate,10)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
        
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Year: #Mid(local.reportDate,7,4)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Day: #Mid(local.reportDate,4,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Month: #Left(local.reportDate,2)#</cfoutput><br />
                        <cfset local.reportDate = CreateDate(Mid(local.reportDate,7,4),Left(local.reportDate,2),Mid(local.reportDate,4,2)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
                        
                        <cfcontinue />
                    </cfif>
                    
                    <!--- 3a. Check for report name, report name is located on 3 separate lines --->
                    <cfset local.re = '^.{54}FOR THE MONTH OF ' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract report name --->
                        <cfset local.reportName = "DETAIL REPORT OF MEMBERS WITH UNRESOLVED DISCREPANCIES 6 MONTHS OR MORE " & Mid(local.line,55,Len(local.line)) /><!--- second parameter causes out of bounds but that's okay; we will truncate the string --->
                        <cfset local.reportName = Trim(local.reportName) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Name: #local.reportName#</cfoutput><br />
                        
                        <cfset local.forMonth = Mid(local.line,72,Len(local.line)) /><!--- second parameter causes out of bounds but that's okay; we will truncate the string --->
                        <cfset local.forMonth = Trim(local.forMonth) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For Month of: #local.forMonth#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 3b. Check for report name, report name is located on 3 separate lines --->
                    <cfset local.re = '^.{42}BASED ON INFORMATION FROM THE MONTH OF ' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract report name --->
                        <cfset local.reportName = local.reportName & Mid(local.line,42,Len(local.line)) /><!--- second parameter causes out of bounds but that's okay; we will truncate the string --->
                        <cfset local.reportName = Trim(local.reportName) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Name: #local.reportName#</cfoutput><br />
                        
                        <cfset local.basedonMonth = Mid(local.line,82,Len(local.line)) /><!--- second parameter causes out of bounds but that's okay; we will truncate the string --->
                        <cfset local.basedonMonth = Trim(local.basedonMonth) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Based on Month: #local.basedonMonth#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 4. Check for agency name --->
                    <cfset local.re = '^.{49}AGENCY :.' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract agency name --->
                        <cfset local.agencyName = Mid(local.line,69,Len(local.line)) /><!--- second parameter causes out of bounds but that's okay --->
                        <cfset local.agencyName = Trim(local.agencyName) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agency Name: #local.agencyName#</cfoutput><br />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- Check for processing organization # - this gets set once and re-used until read again --->
                    <cfset local.re = '^.{11}\d{3}[-]\d{3}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract processing organization # --->
                        <cfset local.procOrgPrefix = Mid(local.line,12,7) />
                         <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proc Org Prefix: #local.procOrgPrefix#</cfoutput><br />
                        <!--- This is the only case where we want to process the rest of the line, so we let the next if statement follow and don't have cfcontinue statement --->
                    </cfif>
                    
                    <!--- 5. Check for proc org #, member name, SSN, CAR CD, number of months and CFT/PRTY --->
                    <cfset local.re = '^.{23}\d{3}.{61}\d{3}[-]\d{2}[-]\d{3}' />
                    <cfset local.match = REFind(local.re,local.line) />
                    <cfif local.match neq 0>
                        <!--- extract processing organization # --->
                        <cfset local.procOrg = local.procOrgPrefix & "-" & Mid(local.line,24,3) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proc Org: #local.procOrg#</cfoutput><br />
                    
                        <!--- extract member name --->
                        <cfset local.memberLastName = Trim(Mid(local.line,37,20)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Last Name: #local.memberLastName#</cfoutput><br />
                        
                        <cfset local.memberFirstName = Trim(Mid(local.line,57,14)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member First Name: #local.memberFirstName#</cfoutput><br />
                        
                        <cfset local.memberMI = Mid(local.line,71,1) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Middle Initial: #local.memberMI#</cfoutput><br />
                        
                        <!--- extract SSN --->
                        <cfset local.SSN = Mid(local.line,88,11) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SSN: #local.SSN#</cfoutput><br />
                        
                        <!--- remove dashes from SSB--->
                        <cfset local.SSN = Replace(local.SSN,"-","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SSN: #local.SSN#</cfoutput><br />
                        
                        <!--- store last 4 digits of SSN --->
                        <cfset local.xSSN = Right(local.SSN,4) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last 4 digits of SSN: #local.xSSN#</cfoutput><br />
                        
                        <!--- we have enough information now to do a look up for UIN --->
                        <cfset local.UIN = application.brrDAO.getUIN(local.SSN,local.memberLastName,local.memberFirstName) />
                        <cfoutput>Return value from DAO.getUIN(): #local.UIN#</cfoutput><br />
                        
                        <cfif local.UIN EQ ""><!--- Set UIN to special format in no UINs are found --->
                            <cfset local.UIN = "&" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
                            <cfset local.systemNotes = "No UIN found" />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
                        </cfif>
                        
						<cfif find("UINs found",local.UIN)><!--- Set UIN to special format if more than 1 UIN is found --->
                        	<cfset local.systemNotes = local.UIN />
                            <cfset local.UIN = "*" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UIN: #local.UIN#</cfoutput><br />
                        
                        <!--- extract CAR CD --->
                        <cfset local.carCd = Mid(local.line,104,2) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;carCd: #local.carCd#</cfoutput><br />
                        
                        <!--- extract Number of months --->
                        <cfset local.noMos = Trim(Mid(local.line,116,4)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number of months: #local.noMos#</cfoutput><br />
                        
                        <!--- extract CFT/PTRY --->
                        <cfset local.cftPtry = Mid(local.line,127,1) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CFT/PTRY: #local.cftPtry#</cfoutput><br />
                        
                        <!--- now that we've read all lines that record should have, we store the information into database --->
                        <cfoutput>Attempting to write a record to database.</cfoutput><br />
                    
						<cfscript>
                            result = application.brrDAO.storeSixMonthsRPTReport(
                                filename=arguments_fileName,
                                reportDate=local.reportDate,
                                reportName=local.reportName,
                                reportNo=local.reportNo,
                                pageNo=local.pageNo,
                                agencyName=local.agencyName,
                                procOrg=local.procOrg,
                                UIN=local.UIN,
                                xSSN=local.xSSN,
                                memberFirstName=local.memberFirstName,
                                memberLastName=local.memberLastName,
                                memberMI=local.memberMI,
                                forMonth=local.forMonth,
                                basedonMonth=local.basedonMonth,
                                CarCD=local.CarCd,
                                noMos=local.noMos,
                                cftPtry=local.cftPtry,
								systemNotes=local.systemNotes
                            );
                            if(result)
                                WriteOutput("Writing to database done: record saved.<br />");
                            else
                                WriteOutput("Writing to database aborted: record was already found.<br />");
                                
                            recordCount = recordCount + 1;
                       </cfscript>
                       
                       <!--- after every 5 records, sleep for 1 7/8 seconds (aka 1875 miliseconds), 
                              so that we parse a record a max of about 2.33/second (aka 160/minute and 9,600/hour) --->
                       <cfif recordCount MOD 5 EQ 0>
                            <cfthread action="sleep" duration="1875"/>
                       </cfif>
                        
                    </cfif>
                    
                </cfloop><!--- END cfloop[pathName] --->
                
                <!--- move file --->
				<cfset application.OBFSFileMgmtFacade.makeFilePermanent(tempFileName=arguments_fileName,permanentFileName=arguments_fileName & " " & #DateFormat(Now(),"yy-mm-dd")# & " " & #TimeFormat(Now(),"hh-mm-ss tt")#) />

                <cfcatch type="any">
                	<!--- the controller will catch and process the error --->
                	<cfrethrow />
                </cfcatch>
            </cftry>
            
		</cfthread>
        
	</cffunction>
    
</cfcomponent>