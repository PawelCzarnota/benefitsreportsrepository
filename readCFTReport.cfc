<!--- readCFTReport.cfc

Purpose:		this component contains functions that read CFT Reports
Called by:		brrController.cfc
				brrDAO.cfc
Includes:		none
Modules:		none
CustomTags:		none
Components:		readCFTReport

Created:		21 June 2012 by Pawel Czarnota
Last Updated:	$Date: 2014-10-23 14:04:30 -0500 (Thu, 23 Oct 2014) $ by $Author: Pawel Czarnota $ - $Rev: 10427 $

SVN URL:		$URL: https://$svnbase/benefitsreportrepository/trunk/readPurgeReport.cfc $  
Decommissioned: 10/2020
--->

<cfcomponent name="readCFTReport" hint="Component to parse CFT reports">

	<cffunction name="init" access="public" output="false" hint="Initialize and return readCFTReport object ">
		
        <cfreturn this />
	</cffunction><!--- END init --->

	<cffunction name="readCFTReport" access="public" output="true" returntype="void" hint="reads data from CFT report">
    	<cfargument name="fileName" type="string" required="yes" hint="name of the report file" />
		<cfargument name="pathName" type="string" required="yes" hint="path to the report file" />
        <cfargument name="threadName" type="string" required="yes" hint="name of the thread this will run under" />  
        
        <cfoutput>Reading CFT Report File: #arguments.fileName# <br /> </cfoutput>
        
        <cfscript>	
			// the processing will be done in a thread
			
			// set up the attributes needed by the thread
			local.attributes = structNew();
			// add all the variables about the upload, the file, and the user
			structInsert(local.attributes, "startTime", now());
			structInsert(local.attributes, "arguments_fileName", arguments.fileName);
			structInsert(local.attributes, "arguments_pathName", arguments.pathName);
			
			//add some application and controller variables the thread will need
			structInsert(local.attributes, "BRRNotificationController", application.BRRNotificationController);
			structInsert(local.attributes, "TechSubject", "Error Processing #arguments.fileName# purge file");
			
			// set the other attributes of the thread 
			structInsert(local.attributes, "threadName", arguments.threadName);
			structInsert(local.attributes, "name", arguments.threadName);
			structInsert(local.attributes, "action", "run");
			// note, the priority is always low
			structInsert(local.attributes, "priority", "Low");
		</cfscript>	
        
        <cfthread attributeCollection="#local.attributes#">
            <cftry>  
            	<cfscript>
            		// this will tell us if there were any problems
					//	 It will be used to tell us if we should or should not send the final success message
					thread.processingError = false;
					thread.fileName = arguments_fileName;
					
					//this variable will be used to make the process sleep
					recordcount = 0;
					
				</cfscript>
        
				<!--- Store each record in data structure --->
                <cfset local.records = ArrayNew(1) />
            
                <!--- indexes to help us set up records data structure --->
                <cfset local.recordNo = 0 /> 
                <cfset local.payperiodNo = 0 /> 
                
                <!--- file contains multiple reports, after we read in first line in first report, we set this to false --->
                <cfset local.firstReport = true />
                
				<cfset local.file = FileOpen(arguments_pathName,"read") />
				
                <cfloop condition="NOT FileisEOF(local.file)">
					
					<!--- read next line --->
					<cfset local.line = FileReadLine(local.file) />
				
                    <cfoutput>#local.line# - Line length: #Len(local.line)#<br /></cfoutput>        
                
                	<cfset local.systemNotes = "" />
                
                    <!--- 1. Check for report number & other variables until we reach record data - uses lookahead --->
                    <cfset local.re = '^?REPORT NO:.{38}DEPARTMENT'>
                    <cfset local.match = REFind(local.re,local.line)>
                    <cfif local.match neq 0>
                    
                        <!--- extract report number from line --->
                        <cfif local.firstReport eq true>
                            <cfset local.reportNo = Left(local.line,25) />
                            <cfset local.reportNo = Trim(Right(local.reportNo,14)) />
                            <cfset local.firstReport = false />
                        <cfelse>
                            <cfset local.reportNo = Left(local.line,25) />
                            <cfset local.reportNo = Trim(Right(local.reportNo,13)) />
                        </cfif>
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Number: #local.reportNo#</cfoutput><br />
                        
                        <!--- extract page number from line --->
                        <cfset local.pageNo = Trim(Right(local.line,7)) />
                        <!---  account for a case when number is in 1,1234 format --->
                        <cfset local.pageNo = Replace(local.pageNo,",","","all") />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page: #local.pageNo#</cfoutput><br />
						
						<!--- read next line --->
						<cfset local.line = FileReadLine(local.file) />
						
						<!--- 2. Record report date --->
						<cfset local.re = '^PROGRAM NO:.'>
						<cfset local.match = REFind(local.re,local.line)>
						<cfif local.match neq 0>
											
							<cfset local.reportDate = Left(local.line,131) />
							<cfset local.reportDate = Right(local.reportDate,8) />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Year: #"20" & Mid(local.reportDate,7,2)#</cfoutput><br />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Day: #Mid(local.reportDate,4,2)#</cfoutput><br />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ReportDate Month: #Left(local.reportDate,2)#</cfoutput><br />
							<cfset local.reportDate = CreateDate("20" & Mid(local.reportDate,7,2),Left(local.reportDate,2),Mid(local.reportDate,4,2)) />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Date: #local.reportDate#</cfoutput><br />
						</cfif>
						
						<!--- read next two lines --->
						<cfset local.line = FileReadLine(local.file) />
						<cfset local.line = FileReadLine(local.file) />
						
						<!--- 3. Check for report name --->
						<cfset local.re = '^.{15}CANDIDATES'>
						<cfset local.match = REFind(local.re,local.line)>
						<cfif local.match neq 0>
							<!--- extract report name --->
							<cfset local.reportName = Trim(Mid(local.line,16,Len(local.line))) /><!--- second parameter causes out of bounds but that's okay --->
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Name: #local.reportName#</cfoutput><br />
							
							<cfset local.forMonth = Trim(Mid(local.line,60,15)) />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For Month: #local.forMonth#</cfoutput><br />
							
							<cfset local.basedonMonth = Trim(Mid(local.line,113,Len(local.line))) /><!--- second parameter causes out of bounds but that's okay --->
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Based on Month: #local.basedonMonth#</cfoutput><br />
						</cfif>
						
						<!--- read next line --->
						<cfset local.line = FileReadLine(local.file) />
						
						<!--- 4. Check for agency name --->
						<cfset local.re = '^.{57}AGENCY :.'>
						<cfset local.match = REFind(local.re,local.line)>
						<cfif local.match neq 0>
							<!--- extract agency name --->
							<cfset local.agencyName = Trim(Mid(local.line,67,Len(local.line))) /><!--- second parameter causes out of bounds but that's okay --->
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agency Name: #local.agencyName#</cfoutput><br />
						</cfif>
						
						<!--- read next three lines --->
						<cfset local.line = FileReadLine(local.file) />
						<cfset local.line = FileReadLine(local.file) />
						<cfset local.line = FileReadLine(local.file) />
						
						<!--- 5. Check for member name, SSN and processing organization --->
						<cfset local.re = '^.{38}\d{3}[-]\d{2}[-]\d{3}'>
						<cfset local.match = REFind(local.re,local.line)>
						<cfif local.match neq 0>
							<!--- extract member name --->
							<cfset local.memberLastName = Trim(Left(local.line,20)) />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Last Name: #local.memberLastName#</cfoutput><br />
							<cfset local.memberFirstName = Left(local.line,34) />
							<cfset local.memberFirstName = Trim(Right(local.memberFirstName,14)) />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member First Name: #local.memberFirstName#</cfoutput><br />
							<cfset local.memberMI = Mid(local.line,35,1) />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Member Middle Initial: #local.memberMI#</cfoutput><br />
							
							<!--- extract SSN --->
							<cfset local.SSN = Left(local.line,49) />
							<cfset local.SSN = Right(local.SSN,11) />
							
							<!--- remove dashes from SSB--->
							<cfset local.SSN = Replace(local.SSN,"-","","all") />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SSN: #local.SSN#</cfoutput><br />
							
							<!--- store last 4 digits of SSN --->
							<cfset local.xSSN = Right(local.SSN,4) />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last 4 digits of SSN: #local.xSSN#</cfoutput><br />
							
							<!--- we have enough information now to do a look up for UIN --->
							<cfset local.UIN = application.brrDAO.getUIN(local.SSN,local.memberLastName,local.memberFirstName) />
							<cfoutput>Return value from DAO.getUIN(): #local.UIN#</cfoutput><br />
							
							<cfif local.UIN EQ ""><!--- Set UIN to special format in no UINs are found --->
								<cfset local.UIN = "&" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
								<cfset local.systemNotes = "No UIN found" />
								<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
							</cfif>
							
							<cfif find("UINs found",local.UIN)><!--- Set UIN to special format if more than 1 UIN is found --->
								<cfset local.systemNotes = local.UIN />
								<cfset local.UIN = "*" & Left(local.memberLastName,2)& Left(local.memberFirstName,2) & Right(local.SSN,4) />
								<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Note: #local.systemNotes#</cfoutput><br />
							</cfif>
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UIN: #local.UIN#</cfoutput><br />
							
							<!--- extract processing organization --->
							<cfset local.procOrg = Left(local.line,63) />
							<cfset local.procOrg = Trim(Right(local.procOrg,11)) />
							<cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proc Org: #local.procOrg#</cfoutput><br />
							
						</cfif>
                    
						<!--- we need to increment record number and clear the payperiod number
							  we do this only if the person's SSN is different - if the SSN is the same as previous person's
							  it means we are still processing the same person and the data spans multiple report pages
							  We need to do a look ahead here
						--->
						<cfif local.recordNo EQ 0 OR local.UIN NEQ local.records[local.recordNo].UIN>
							<cfset local.recordNo = local.recordNo + 1 />
							<cfset local.payperiodNo = 0 />
							<cfset local.records[local.recordNo] = StructNew() />
							<cfset local.records[local.recordNo].payPeriod = ArrayNew(1) />
							<!--- preset variables that might not be read from file --->
							<cfset local.records[local.recordNo].hlthInsTotal = "$.00" />
							<cfset local.records[local.recordNo].dntlInsTotal = "$.00" />
							<cfset local.records[local.recordNo].lifeInsTotal = "$.00" />
							<!--- set tempPeriodNo to 1, used later to update pay period record type to dental, health, or life --->
							<cfset local.tempPeriodNo = 1 />
							<cfset local.records[local.recordNo].pageNo = local.pageNo />
						</cfif>
						
						<!--- store all variables we have read so far --->
						<cfset local.records[local.recordNo].fileName = arguments_fileName />
						<cfset local.records[local.recordNo].reportNo = local.reportNo />
						<cfset local.records[local.recordNo].reportDate = local.reportDate />
						<cfset local.records[local.recordNo].reportName = local.reportName />
						<cfset local.records[local.recordNo].forMonth = local.forMonth />
						<cfset local.records[local.recordNo].basedonMonth = local.basedonMonth />
						<cfset local.records[local.recordNo].agencyName = local.agencyName />
						<cfset local.records[local.recordNo].memberLastName = local.memberLastName />
						<cfset local.records[local.recordNo].memberFirstName = local.memberFirstName />
						<cfset local.records[local.recordNo].memberMI = local.memberMI />
						<cfset local.records[local.recordNo].xSSN = local.xSSN />
						<cfset local.records[local.recordNo].systemNotes = local.systemNotes />
						<cfset local.records[local.recordNo].UIN = local.UIN />
						<cfset local.records[local.recordNo].procOrg = local.procOrg />
						
						<continue />
                    </cfif><!--- end check 1. and end lookahead --->
					
                    <!--- 6. Check for Pay Period --->
                    <cfset local.re = '^\d{2}[/]\d{2}[/]\d{4}.{6}[/].{2}[/]'>
                    <cfset local.match = REFind(local.re,local.line)>
                    <cfif local.match neq 0>
                        <!--- increase pay periodNo --->
                        <cfset local.payperiodNo = local.payperiodNo + 1 /> 
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo] = StructNew() />
                        
                        <!--- account for a situation where a record is on multiple pages and we won't know what type it is--->
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].recordType = "Unknown" />
                        
                        <!--- extract Pay period --->
                        <cfset local.payPeriodDate = Left(local.line,10) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period: #local.payPeriodDate#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period Year: #Mid(local.payPeriodDate,7,4)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period Day: #Mid(local.payPeriodDate,4,2)#</cfoutput><br />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period Month: #Left(local.payPeriodDate,2)#</cfoutput><br />
                        <cfset local.payPeriodDate = CreateDate(Mid(local.payPeriodDate,7,4),Left(local.payPeriodDate,2),Mid(local.payPeriodDate,4,2)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Period: #local.payPeriodDate#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].payPeriodDate = local.payPeriodDate />
                        
                        <!--- extract membership file --->
                        <cfset local.membershipFile = Left(local.line,24) />
                        <cfset local.membershipFile = Trim(Right(local.membershipFile,12)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Membership File: #local.membershipFile#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].membershipFile = local.membershipFile />
                        
                        <!--- extract basic life units --->
                        <cfset local.basicLifeUnits = Left(local.line,33) />
                        <cfset local.basicLifeUnits = Trim(Right(local.basicLifeUnits,7)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Basic Life Units: #local.basicLifeUnits#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].basicLifeUnits = local.basicLifeUnits />
                        
                        <!--- extract pt percentage --->
                        <cfset local.ptPerc = Left(local.line,39) />
                        <cfset local.ptPerc = Trim(Right(local.ptPerc,6)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PT Percentage: #local.ptPerc#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].ptPerc = local.ptPerc />
                        
                        <!--- extract membership liability --->
                        <cfset local.membershipLiability = Left(local.line,54) />
                        <cfset local.membershipLiability = Trim(Right(local.membershipLiability,15)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Membership Liability: #local.membershipLiability#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].membershipLiability = local.membershipLiability />
                        
                        <!--- extract pay record & add a note if necessary --->
                        <cfset local.payRecord = Left(local.line,65) />
                        <cfset local.payRecord = Right(payRecord,11) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Record: #local.payRecord#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].payRecord = local.payRecord />
                        <cfif local.payRecord eq "    /  /   ">
                            <cfset local.payRecordNote = "** NO PAYROLL INFORMATION FOR PAY PERIOD" />
                            <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pay Record Note: #local.payRecordNote#</cfoutput><br /    
                        <cfelse>
                            <cfset local.payRecordNote = "" />
                        </cfif>
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].payRecordNote = local.payRecordNote />
                        
                        <!--- extract basic life units #2 --->
                        <cfset local.basicLifeUnits2 = Left(local.line,73) />
                        <cfset local.basicLifeUnits2 = Trim(Right(local.basicLifeUnits2,8)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Basic Life Units ##2: #local.basicLifeUnits2#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].basicLifeUnits2 = local.basicLifeUnits2 />
                        
                        <!--- extract pt percentage #2 --->
                        <cfset local.ptPerc2 = Left(local.line,79) />
                        <cfset local.ptPerc2 = Trim(Right(local.ptPerc2,6)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PT Percentage ##2: #local.ptPerc2#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].ptPerc2 = local.ptPerc2 />
                        
                        <!--- extract payroll deduction --->
                        <cfset local.payDed = Left(local.line,99) />
                        <cfset local.payDed = Trim(Right(local.payDed,20)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payroll Deduction: #local.payDed#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].payDed = local.payDed />
                        
                        <!--- extract total member difference --->
                        <cfset local.totalMemberDiff = Left(local.line,113) />
                        <cfset local.totalMemberDiff = Trim(Right(local.totalMemberDiff,14)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Member Difference: #local.totalMemberDiff#</cfoutput><br />
                        <cfset local.records[local.recordNo].payPeriod[local.payperiodNo].totalMemberDiff = local.totalMemberDiff />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 7. Check for dental insurance total --->
                    <cfset local.re = '^.{73}DENTAL INSURANCE TOTAL'>
                    <cfset local.match = REFind(local.re,local.line)>
                    <cfif local.match neq 0>
                        <!--- extract dental insurance total --->
                        <cfset local.dntlInsTotal = Left(local.line,Len(line)) />
                        <cfset local.dntlInsTotal = Trim(Right(local.dntlInsTotal,13)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dental Insurance Total: #local.dntlInsTotal#</cfoutput><br />
                        <cfset local.records[local.recordNo].dntlInsTotal = local.dntlInsTotal />
                        
                        <!--- mark all pay period information read before this line as Dental records --->
                        <cfloop from="#local.tempPeriodNo#" to="#local.payperiodNo#" index="local.i">
                            <cfset local.records[local.recordNo].payPeriod[local.i].recordType = "Dental" />
                        </cfloop>
                        
                        <cfset local.tempPeriodNo = local.payperiodNo + 1 />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 8. Check for health insurance total --->
                    <cfset local.re = '^.{73}HEALTH INSURANCE TOTAL'>
                    <cfset local.match = REFind(local.re,local.line)>
                    <cfif local.match neq 0>
                        <!--- extract heathl insurance total --->
                        <cfset local.hlthInsTotal = Left(local.line,Len(line)) />
                        <cfset local.hlthInsTotal = Trim(Right(local.hlthInsTotal,13)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Health Insurance Total: #local.hlthInsTotal#</cfoutput><br />
                        <cfset local.records[local.recordNo].hlthInsTotal = local.hlthInsTotal />
                        
                        <!--- mark all pay period information read before this line as Health records --->
                        <cfloop from="#local.tempPeriodNo#" to="#local.payperiodNo#" index="local.i">
                            <cfset local.records[local.recordNo].payPeriod[local.i].recordType = "Health" />
                        </cfloop>
                        
                        <cfset local.tempPeriodNo = local.payperiodNo + 1 />
                        
                        <cfcontinue /> 
                    </cfif>
                    
                    <!--- 8. Check for life insurance total --->
                    <cfset local.re = '^.{73}LIFE INSURANCE TOTAL'>
                    <cfset local.match = REFind(local.re,local.line)>
                    <cfif local.match neq 0>
                        <!--- extract heathl insurance total --->
                        <cfset local.lifeInsTotal = Left(local.line,Len(line)) />
                        <cfset local.lifeInsTotal = Trim(Right(local.lifeInsTotal,13)) />
                        <cfoutput>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Life Insurance Total: #local.lifeInsTotal#</cfoutput><br />
                        <cfset local.records[local.recordNo].lifeInsTotal = local.lifeInsTotal />
                        
                        <!--- mark all pay period information read before this line as Health records --->
                        <cfloop from="#local.tempPeriodNo#" to="#local.payperiodNo#" index="local.i">
                            <cfset local.records[local.recordNo].payPeriod[local.i].recordType = "Life" />
                        </cfloop>
                        
                        <cfset local.tempPeriodNo = local.payperiodNo + 1 />
                        
                        <cfcontinue /> 
                    </cfif>
                        
                </cfloop><!--- END cfloop[pathName] --->
                
				<cfset FileClose(local.file) />
				
                <!--- add these records into database, if they already exist, update them --->
                <cfloop index="local.i" from="1" to="#ArrayLen(local.records)#">
                    
                    <!--- check if payperiod record type is defined and if it's unknown, we need to do change it based on the next record type found (this will work since records are read sequentially) --->
                    <cfloop index="local.payPeriodNumber" from="1" to="#ArrayLen(local.records[local.i].payPeriod)#">
                    	<cfif local.records[local.i].payPeriod[local.payPeriodNumber].recordType EQ "Unknown" >
                        	<!--- Set this to the next record's first pay period's record type. If this doesn't exist, we cannot do anything more for this record and probram will throw an error --->
							<cfset local.records[local.i].payPeriod[local.payPeriodNumber].recordType = local.records[local.i+1].payPeriod[1].recordType />
                            <cfif local.records[local.i+1].lifeInsTotal NEQ "$.00">
                            	<cfset local.records[local.i].lifeInsTotal = local.records[local.i+1].lifeInsTotal />
                            </cfif>
                            <cfif local.records[local.i+1].hlthInsTotal NEQ "$.00">
								<cfset local.records[local.i].hlthInsTotal = local.records[local.i+1].hlthInsTotal />
                            </cfif>
                            <cfif local.records[local.i+1].dntlInsTotal NEQ "$.00">
                            	<cfset local.records[local.i].dntlInsTotal = local.records[local.i+1].dntlInsTotal />
                            </cfif>
                            <!--- We might need to also update the next record with life, health and dental information--->
                            <cfif local.records[local.i].lifeInsTotal NEQ "$.00">
                            	<cfset local.records[local.i+1].lifeInsTotal = local.records[local.i].lifeInsTotal />
                            </cfif>
                            <cfif local.records[local.i].hlthInsTotal NEQ "$.00">
								<cfset local.records[local.i+1].hlthInsTotal = local.records[local.i].hlthInsTotal />
                            </cfif>
                            <cfif local.records[local.i].dntlInsTotal NEQ "$.00">
                            	<cfset local.records[local.i+1].dntlInsTotal = local.records[local.i].dntlInsTotal />
                            </cfif>
                        </cfif>
                    </cfloop>
                    
                    <!--- write to database --->
					<cfoutput>Attempting to write a record to database.</cfoutput><br />
                    
                    <cfscript>
                        result = application.brrDAO.storeCFTReport(
                         filename=local.records[local.i].fileName,
                         reportDate=local.records[local.i].reportDate,
                         reportName=local.records[local.i].reportName,
                         reportNo=local.records[local.i].reportNo,
                         pageNo=local.records[local.i].pageNo,
                         agencyName=local.records[local.i].agencyName,
                         procOrg=local.records[local.i].procOrg,
                         UIN=local.records[local.i].UIN,
                         xSSN=local.records[local.i].xSSN,
                         memberFirstName=local.records[local.i].memberFirstName,
                         memberLastName=local.records[local.i].memberLastName,
                         memberMI=local.records[local.i].memberMI,
                         payPeriod=local.records[local.i].payPeriod,
                         dntlInsTotal=local.records[local.i].dntlInsTotal,
                         hlthInsTotal=local.records[local.i].hlthInsTotal,
						 lifeInsTotal=local.records[local.i].lifeInsTotal,
                         forMonth=local.records[local.i].forMonth,
                         basedonMonth=local.records[local.i].basedonMonth,
						 systemNotes=local.records[local.i].systemNotes
                        );
                    
                        if(result)
                            WriteOutput("Writing to database done: record saved.<br />");
                        else
                            WriteOutput("Writing to database aborted: record was already found.<br />");
                    </cfscript>
                    
					<cfset recordCount = recordCount + 1 />
                       
				    <!--- after every 5 records, sleep for 1 7/8 seconds (aka 1875 miliseconds), 
                      so that we parse a record a max of about 2.33/second (aka 160/minute and 9,600/hour) --->
                    <cfif recordCount MOD 5 EQ 0>
                        <cfthread action="sleep" duration="1875"/>
                    </cfif>
                    
                </cfloop><!--- END cfloop[records] --->
        		
                Here is the list of the records that were stored in the database:
                <cfdump var="#local.records#" />
                
                <!--- move file --->
				<cfset application.OBFSFileMgmtFacade.makeFilePermanent(tempFileName=arguments_fileName,permanentFileName=arguments_fileName & " " & #DateFormat(Now(),"yy-mm-dd")# & " " & #TimeFormat(Now(),"hh-mm-ss tt")#) />

                <cfcatch type="any">
                	<!--- the controller will catch and process the error --->
                	<cfrethrow />
                </cfcatch>
            </cftry>
            
		</cfthread>
        
	</cffunction><!--- END readCFTReport --->
    
</cfcomponent>